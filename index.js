// Mock database
const key = 's48-data'
let posts = JSON.parse(localStorage.getItem(key)) ?? []
let count = posts.length > 0 ? posts[posts.length - 1].id + 1 : 0;
const setLocalStorage = (newData) => {
    localStorage.setItem(key, JSON.stringify(newData));
}

// ADD POST DATA
document.querySelector("#form-add-post").addEventListener("submit", (e) => {
    // Prevent the page from reloading
    // Prevents default behavior of event
    e.preventDefault();

    posts.push({
        id: count, 
        title: document.querySelector("#txt-title").value,
        body: document.querySelector("#txt-body").value
    });
    count++

    alert("Post successfully added!")
    setLocalStorage(posts)
    showPosts()
})

// RETRIEVE POSTS
const showPosts = () => {
    let postEntries = ""
    posts.forEach((post) => {
        postEntries += `
            <div id="post-${post.id}">
                <h3 id="post-title-${post.id}">${post.title}</h3>
                <p id="post-body-${post.id}">${post.body}</p>
                <button onClick="editPost('${post.id}')">Edit</button>
                <button onClick="deletePost('${post.id}')">Delete</button>
            </div
        `
    })
    document.querySelector("#div-post-entries").innerHTML = postEntries
}

// EDIT POST
const editPost = (id) => {
    const title = document.querySelector(`#post-title-${id}`).innerHTML
    const body = document.querySelector(`#post-body-${id}`).innerHTML

    document.querySelector("#form-edit-post").style.display = "block"
    document.querySelector("#form-edit-post-note").style.display = "none"
    document.querySelector("#txt-edit-id").value = id
    document.querySelector("#txt-edit-title").value = title
    document.querySelector("#txt-edit-body").value = body
};


// UPDATE POST

document.querySelector("#form-edit-post").addEventListener("submit", (e) => {
    e.preventDefault()
    for (let i = 0; i < posts.length; i++) {
        if (posts [i].id.toString() === document.querySelector("#txt-edit-id").value) {
            posts[i].title = document.querySelector("#txt-edit-title").value
            posts[i].body = document.querySelector("#txt-edit-body").value
            alert("Successfully updated!")
            break
        }
    }
    setLocalStorage(posts)
    showPosts(posts)
})

// DELETE POST

const deletePost = (id) => {
    let targetIndex = null
    for (let i = 0; i < posts.length; i++) {
        if (posts [i].id.toString() === id) {
            targetIndex = i;
            break;
        }
    }

    if (targetIndex !== null) {
        posts.splice(targetIndex, 1)
        alert("Successfully deleted!")
        setLocalStorage(posts)
        showPosts()
    }
}

showPosts()